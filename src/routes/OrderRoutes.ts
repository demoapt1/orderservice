import { OrderController } from "../controller/OrderController";

export const OrderRoutes = [{
    method: "get",
    route: "/orders",
    controller: OrderController,
    action: "all"
}, {
    method: "get",
    route: "/orders/:id",
    controller: OrderController,
    action: "one"
}, {
    method: "post",
    route: "/orders",
    controller: OrderController,
    action: "save"
}, {
    method: "delete",
    route: "/orders",
    controller: OrderController,
    action: "remove"
}];