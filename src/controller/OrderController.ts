// import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {OrderData} from "../data/OrderData";

export class OrderController {

    private orderData = new OrderData();

    async all(request: Request, response: Response, next: NextFunction) {
        try {
            let orders = await this.orderData.all(request.params);
            response.json({status: 0, orders: orders});
        } catch (err) {
            console.log(err);
            response.json({status: 1, error: 'failed to fetch orders'});
        }
    }

    async one(request: Request, response: Response, next: NextFunction) {
        try {
            let order = await this.orderData.one(request.params);
            response.json({status: 0, orders: order});
        } catch(err) {
            console.log(err);
            response.json({status: 1, error: 'failed to fetch order'});
        }
    }

    async save(request: Request, response: Response, next: NextFunction) {
        try {
            let order = await this.orderData.save(request.body);
            response.json({status: 0, orders: order})
        } catch(err) {
            console.log(err);
            response.json({status: 1, error: 'failed to save order'});
        }
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        try {
            await this.orderData.remove(request.body);
            response.json({ status: 0, message: 'the order has been removed' });
        } catch (err) {
            console.log(err);
            response.json({ status: 1, message: 'failed to remove the order' });
        }
    }

}